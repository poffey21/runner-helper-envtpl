FROM gitlab/gitlab-runner-helper:x86_64-latest

# File is retrieved via wget in the GitLab Job.
COPY envtpl-linux-amd64 /usr/bin/envtpl
RUN chmod +x /usr/bin/envtpl
ENTRYPOINT if [ -d "${CI_PROJECT_DIR}" ] && [[ -z "${ENABLE_TEMPLATE_ENGINE}" ]]; then find ${CI_PROJECT_DIR} -name "*.tpl" -exec envtpl {} \;; fi && /bin/bash
