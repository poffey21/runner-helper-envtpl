# Environment Template Runner Helper

GitLab Runner Helper container is used to prepare the container your job uses by downloading source code, cache, artifacts, etc...

This Runner Helper variable also takes all `.tpl` files and converts them into regular files using the provided GitLab CI Variables.

## Usage


**Either using Kubernetes**

`.gitlab-ci.yml`

```yaml
job:
  variables:
    KUBERNETES_HELPER_IMAGE: 'registry.gitlab.com/poffey21/runner-helper-envtpl:latest'
    ENABLE_TEMPLATE_ENGINE: 1
  script:
  - cat templated_file.ini.tpl
```

**Or using Docker**

`config/config.toml`:

```yaml
  # ...
  [runners.docker]
    helper_image = "registry.gitlab.com/poffey21/runner-helper-envtpl:poffey21-master-patch-55048"
```

`.gitlab-ci.yml`

```yaml
job:
  variables:
    ENABLE_TEMPLATE_ENGINE: 1
  script:
  - cat templated_file.ini.tpl
```


#### Resources

[Issue showing how to create runner](https://gitlab.com/gitlab-org/gitlab-runner/issues/3371)

[envtpl Go Implementation](https://github.com/mattrobenolt/envtpl)

[Reverse Engineer Container](https://github.com/LanikSJ/dfimage) (didn't actually work)

[How to Run Bash Command After Container Starts](https://forums.docker.com/t/how-to-run-bash-command-after-startup/21631)
